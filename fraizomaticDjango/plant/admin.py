from django.contrib import admin
from .models import Planter, Plant, LifeCycle
# Register your models here.

admin.site.register(Planter)
admin.site.register(Plant)
admin.site.register(LifeCycle)
