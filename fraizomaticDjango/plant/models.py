from django.db import models

# Create your models here.
class Plant(models.Model):
    name = models.CharField(max_length=255)
    lifecycle = models.ForeignKey("LifeCycle", on_delete=models.CASCADE)

    def __str__(self):
        return self.name

class LifeCycle(models.Model):
    days_before_maturity = models.IntegerField()
    days_of_productivity = models.IntegerField()
    sunny_hours_by_day = models.IntegerField()
    optimum_mid_temp = models.FloatField()
    max_temp = models.FloatField()
    min_temp = models.FloatField()
    optimum_humidity = models.FloatField()
    min_humidity = models.FloatField()
    max_humidity = models.FloatField()
    optimum_luminosity = models.FloatField()

class Planter(models.Model):
    name = models.CharField(max_length=255, default="")
    plant = models.ForeignKey("Plant", on_delete=models.CASCADE)
    is_light_swicthed_on = models.BooleanField()
    is_solenoid_valve_opened = models.BooleanField()
    water_level = models.FloatField()

    def __str__(self):
        return self.name if self.name else str(self.id)
