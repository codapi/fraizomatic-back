from rest_framework import serializers
from .models import Planter, LifeCycle, Plant

class DatasSentSerializer(serializers.Serializer):
    hygrometry = serializers.FloatField()
    temperature = serializers.FloatField()
    brightness = serializers.FloatField()
    planter = serializers.IntegerField()

class LifeCycleSerializer(serializers.ModelSerializer):
    class Meta:
        model = LifeCycle
        fields = [
            'id',
            'days_before_maturity',
            'days_of_productivity',
            'sunny_hours_by_day',
            'optimum_mid_temp',
            'max_temp',
            'min_temp',
            'optimum_humidity',
            'min_humidity',
            'max_humidity',
            'optimum_luminosity',
        ]

class PlantSerializer(serializers.ModelSerializer):

    lifecycle = LifeCycleSerializer()

    class Meta:
        model = Plant
        fields = [
            'id',
            'name',
            'lifecycle'
        ]


class PlanterSerializer(serializers.ModelSerializer):
    plant = PlantSerializer()
    class Meta:
        model = Planter
        fields = [
            'id',
            'name',
            'plant',
            'is_light_swicthed_on',
            'is_solenoid_valve_opened',
            'water_level'
        ]
