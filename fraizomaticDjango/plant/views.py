import random
import datetime
from django.utils import timezone
from django.shortcuts import render
from rest_framework.decorators import api_view
from rest_framework.response import Response
from rest_framework.viewsets import ModelViewSet
from .serializers import DatasSentSerializer, PlanterSerializer, PlantSerializer, LifeCycleSerializer
from django.shortcuts import get_object_or_404
from .models import Planter, Plant, LifeCycle

from graphs.models import TempMeasurement, HygroMeasurement, BrightnessMeasurement


# Create your views here.
@api_view(http_method_names=['POST', 'GET'])
def send_information(request):
    serializer = DatasSentSerializer(data=request.data)
    if not serializer.is_valid():
        Response(status=400)
    
    planter = get_object_or_404(Planter, pk=int(serializer.data['planter']))
    lifecycle = planter.plant.lifecycle

    try:
        TempMeasurement.objects.create(temp=serializer.data['temperature'], planter=planter)
        HygroMeasurement.objects.create(hygrometry=serializer.data['hygrometry'], planter=planter)
        BrightnessMeasurement.objects.create(brightness=serializer.data['brightness'], planter=planter)

        first_correct_bm = BrightnessMeasurement.objects.filter(
            datetime__gt=timezone.now().replace(hour=0, minute=0, second=0), 
            brightness__gte=lifecycle.optimum_luminosity).first()
        
        light = light_or_not(first_correct_bm, serializer.data['brightness'], lifecycle)
        solenoid = spray_water_or_not(serializer.data['hygrometry'], lifecycle)

        planter.is_solenoid_valve_opened = solenoid
        planter.is_light_swicthed_on = light
        planter.save()
    except:
        return Response({
            "light": 1 if planter.is_light_swicthed_on else 0,
            "solenoid": 1 if planter.is_solenoid_valve_opened else 0
        })
    return Response({
        "light": 1 if light else 0,
        "solenoid": 1 if solenoid else 0
    })

# Create your views here.
@api_view(http_method_names=['GET'])
def toto_chez_le_veto(request):
    return Response({"message": "C'est toto sur un bateau qui s'est levé tôt pour aller chez le véto soigner son éléphanteau qui s'est pris un coup de marteau à cause d'Antho. "})


class PlanterViewSet(ModelViewSet):
    queryset = Planter.objects.all()
    serializer_class = PlanterSerializer
    permission_classes = []
    
class PlantViewSet(ModelViewSet):
    queryset = Plant.objects.all()
    serializer_class = PlantSerializer
    permission_classes = []

class LifeCycleViewSet(ModelViewSet):
    queryset = LifeCycle.objects.all()
    serializer_class = LifeCycleSerializer
    permission_classes = []

def spray_water_or_not(hygrometry, lifecycle):
    if hygrometry <= lifecycle.optimum_humidity:
        return True
    return False

def light_or_not(first_correct_bm, last_brightness_val, lifecycle):
    # while there is not enough luminosity, we don't turn on the light. We wait for the rising sun
    if not first_correct_bm:
        return False

    # If natural luminosity is bright enough
    if last_brightness_val >= lifecycle.optimum_luminosity:
        return False

    # When the sun rised
    now = timezone.now()
    delta_hours = now - first_correct_bm.datetime

    # if natural luminosity is not bright enough and the daily luminosity is not sufficient
    if int(delta_hours.seconds / 60 / 60) < lifecycle.sunny_hours_by_day:
        return True
    return False