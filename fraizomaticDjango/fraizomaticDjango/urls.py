"""fraizomaticDjango URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, re_path, include
from plant.views import send_information, toto_chez_le_veto, PlanterViewSet, PlantViewSet, LifeCycleViewSet
from graphs.views import hygrometry_view, temperature_view, brightness_view
from rest_framework.routers import DefaultRouter

router = DefaultRouter()
router.register(r'api/planters', PlanterViewSet, basename='planter')
router.register(r'api/plants', PlantViewSet, basename="plant")
router.register(r'api/lifecycle', LifeCycleViewSet, basename="lifecycle")

urlpatterns = [
    path('', include(router.urls)),
    path('admin/', admin.site.urls),
    path('api/send_information', send_information),
    path('toto_chez_le_veto', toto_chez_le_veto),
    re_path('api/hygro_by_planter/(?P<id_planter>[0-9]+)',hygrometry_view, name='hygro_graph'),
    re_path('api/temp_by_planter/(?P<id_planter>[0-9]+)', temperature_view, name='temp_graph'),
    re_path('api/bright_by_planter/(?P<id_planter>[0-9]+)', brightness_view, name='bright_graph'),
]
