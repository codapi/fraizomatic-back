from django.test import TestCase
from .views import group_by_time, avg_by_group
from .models import TempMeasurement
from datetime import datetime

# Create your tests here.
class AnimalTestCase(TestCase):

    def setUp(self):
        self.test = [
            {'datetime': datetime(2020, 1, 1, 1, 1,1)}, # first
            {'datetime': datetime(2020, 1, 1, 1, 1,1)}, # same array
            {'datetime': datetime(2020, 1, 1, 1, 2,1)}, # 1 minute later
            {'datetime': datetime(2020, 1, 1, 2, 1,1)}, # 1 hour later
            {'datetime': datetime(2020, 1, 2, 1, 1,1)}, # 1 day later
            {'datetime': datetime(2020, 2, 1, 1, 1,1)}, # 1 month later
            {'datetime': datetime(2021, 1, 1, 1, 1,1)}, # 1 year later
            {'datetime': datetime(2021, 1, 1, 1, 1,1)}, # 1 year later same array
        ]
    def test_group_by_time(self):
        good_value = [
            [
                {'datetime': datetime(2020, 1, 1, 1, 1, 1)},
                {'datetime': datetime(2020, 1, 1, 1, 1, 1)}
            ],
            [
                {'datetime': datetime(2020, 1, 1, 1, 2, 1)}
            ],
            [
                {'datetime': datetime(2020, 1, 1, 2, 1, 1)}
            ],
            [{'datetime': datetime(2020, 1, 2, 1, 1, 1)}],
            [{'datetime': datetime(2020, 2, 1, 1, 1, 1)}],
            [
                {'datetime': datetime(2021, 1, 1, 1, 1, 1)},
                {'datetime': datetime(2021, 1, 1, 1, 1, 1)}
            ]
        ]
        value = group_by_time(self.test, 'datetime')
        self.assertEqual(value, good_value)
    
    def test_avg_by_group(self):
        test_val = [
            [
                {'val': 3, 'test': 'bonjour'}, {'val': 4, 'test': 'essai'}
            ],
            [
                {'val': 1, 'test': 'essai'}, {'val': 20, 'test': 'essai'}
            ],
        ]

        good_value = [{'val': 3.5, 'test': 'bonjour'}, {'val': 10.5, 'test': 'essai'}]

        value = avg_by_group(test_val, 'val', ['test'])

        self.assertEqual(good_value, value)
        
