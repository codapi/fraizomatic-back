from rest_framework import serializers
from .models import BrightnessMeasurement, HygroMeasurement, TempMeasurement
from plant.serializers import PlanterSerializer

class HygrometrySerializer(serializers.ModelSerializer):
    planter = PlanterSerializer()
    class Meta:
        model = HygroMeasurement
        fields = ['id','hygrometry', 'datetime', 'planter']

class TemperatureSerializer(serializers.ModelSerializer):
    planter = PlanterSerializer()
    class Meta:
        model = TempMeasurement
        fields = ['id', 'temp', 'datetime', 'planter']

class BrightnessSerializer(serializers.ModelSerializer):
    planter = PlanterSerializer()
    class Meta:
        model = BrightnessMeasurement
        fields = ['id','brightness', 'datetime', 'planter']


class HygrometrySerializerNotModels(serializers.Serializer):
    planter = PlanterSerializer()
    hygrometry = serializers.FloatField()
    datetime = serializers.DateTimeField()

class TemperatureSerializerNotModel(serializers.Serializer):
    planter = PlanterSerializer()
    temp = serializers.FloatField()
    datetime = serializers.DateTimeField()

class BrightnessSerializerNotModel(serializers.Serializer):
    planter = PlanterSerializer()
    brightness = serializers.FloatField()
    datetime = serializers.DateTimeField()