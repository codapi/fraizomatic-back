from django.db import models
from plant.models import Planter

# Create your models here.
class TempMeasurement(models.Model):
    temp = models.FloatField()
    datetime = models.DateTimeField(auto_now_add=True)
    planter = models.ForeignKey(Planter, on_delete=models.CASCADE)

class HygroMeasurement(models.Model):
    hygrometry = models.IntegerField()
    datetime = models.DateTimeField(auto_now_add=True)
    planter = models.ForeignKey(Planter, on_delete=models.CASCADE)

class BrightnessMeasurement(models.Model):
    brightness = models.FloatField()
    datetime = models.DateTimeField(auto_now_add=True)
    planter = models.ForeignKey(Planter, on_delete=models.CASCADE)


