import datetime
from django.shortcuts import render
from django.db.models.functions import Trunc
from rest_framework.decorators import api_view, authentication_classes
from rest_framework.response import Response
from django.shortcuts import get_object_or_404
from rest_framework import viewsets
from .models import HygroMeasurement, TempMeasurement, BrightnessMeasurement
from .serializers import HygrometrySerializer, TemperatureSerializer, BrightnessSerializer, HygrometrySerializerNotModels, TemperatureSerializerNotModel
from rest_framework import permissions

def group_by_time(list_values, group_by_key):
    end_list = [[]]
    index = 0
    last_current_key = ''
    for val in list_values:
        # get current key
        if isinstance(val, dict):
            current_key = str(datetime.datetime.timestamp(val[group_by_key]))
        else:
            current_key = str(datetime.datetime.timestamp(getattr(val, group_by_key)))
        
        # if this is the first try
        if last_current_key == '':
            last_current_key = current_key

        # If we go to the next value
        if last_current_key != current_key:
            index += 1
            end_list.append([])
            last_current_key = current_key
        
        end_list[index].append(val)
    
    return end_list

def avg_by_group(list_values, key_to_avg, fields_to_keep):
    end_list = []
    for val in list_values:
        value_to_return = {}
        if isinstance(val[0], dict):
            test = [item[key_to_avg] for item in val]
        else:
            test = [getattr(item, key_to_avg) for item in val]
        nbr_item = len(val)
        avg = sum(test) / nbr_item
        for field in fields_to_keep:
            if isinstance(val[0], dict):
                value_to_return[field] = val[0][field]
            else:
                value_to_return[field] = getattr(val[0], field)
        value_to_return[key_to_avg] = avg
        end_list.append(value_to_return)
    
    return end_list


# Create your views here.
@api_view(http_method_names=['GET'])
@authentication_classes([])
def hygrometry_view(request, id_planter):
    hygros = HygroMeasurement.objects.filter(planter__id=int(id_planter))
    if request.GET.get('by_time', None) == 'hour':
        hygros = hygros.annotate(time=Trunc('datetime', 'hour'))
        list_values = group_by_time(hygros, 'time')
        avg_group = avg_by_group(list_values, 'hygrometry', ['planter', 'datetime'])
        serializer = HygrometrySerializerNotModels(avg_group, many=True)
        return Response(serializer.data)
    elif request.GET.get('by_time', None) == 'minute':
        hygros = hygros.annotate(time=Trunc('datetime', 'minute'))
        list_values = group_by_time(hygros, 'time')
        avg_group = avg_by_group(list_values, 'hygrometry', ['planter', 'datetime'])
        serializer = HygrometrySerializerNotModels(avg_group, many=True)
        return Response(serializer.data)
    else:
        serializer = HygrometrySerializer(hygros, many=True)
        return Response(serializer.data)

@api_view(http_method_names=['GET'])
@authentication_classes([])
def temperature_view(request, id_planter):
    temps = TempMeasurement.objects.filter(planter__id=int(id_planter))
    if request.GET.get('by_time', None) == 'hour':
        temps = temps.annotate(time=Trunc('datetime', 'hour'))
        list_values = group_by_time(temps, 'time')
        avg_group = avg_by_group(list_values, 'temp', ['planter', 'datetime'])
        serializer = TemperatureSerializerNotModel(avg_group, many=True)
        return Response(serializer.data)
    elif request.GET.get('by_time', None) == 'minute':
        temps = temps.annotate(time=Trunc('datetime', 'minute'))
        list_values = group_by_time(temps, 'time')
        avg_group = avg_by_group(list_values, 'temp', ['planter', 'datetime'])
        serializer = TemperatureSerializerNotModel(avg_group, many=True)
        return Response(serializer.data)
    else:
        serializer = TemperatureSerializer(temps, many=True)
        return Response(serializer.data)
    

@api_view(http_method_names=['GET'])
@authentication_classes([])
def brightness_view(request, id_planter):
    brights = BrightnessMeasurement.objects.filter(planter__id=int(id_planter))
    serializer = BrightnessSerializer(brights, many=True)
    return Response(serializer.data)